//
// Created by Lukas Kreis on 13.11.17.
//
#include <utility>
#include <TCanvas.h>
#include <TFile.h>
#include <TLegend.h>

#include "CorrelationTask.h"
#include "Base/DataContainerHelper.h"

CorrelationTask::CorrelationTask(std::string filelist, std::string treename) :
    in_tree_(this->MakeChain(std::move(filelist), treename)),
    reader_(new TTreeReader(in_tree_.get())) { }

void CorrelationTask::Configure(Qn::CorrelationManager &a) {

  auto scalar = [](const std::vector<Qn::QVector> &a) -> double {
    return a[0].x(2)*a[1].x(2) + a[0].y(2)*a[1].y(2);
  };
  auto XY = [](const std::vector<Qn::QVector> &a) {
    return 2*a[0].x(1)*a[1].y(1);
  };
  auto YX = [](const std::vector<Qn::QVector> &a) {
    return 2*a[0].y(1)*a[1].x(1);
  };
  auto XX = [](const std::vector<Qn::QVector> &a) {
    return 2*a[0].x(1)*a[1].x(1);
  };
  auto YY = [](const std::vector<Qn::QVector> &a) {
    return 2*a[0].y(1)*a[1].y(1);
  };
  
  auto Y2XY = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(2)*a[1].x(1)* a[2].y(1);
  };
  auto Y2YX = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(2)*a[1].y(1)* a[2].x(1);
  };
  auto X2XX = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(2)*a[1].x(1)* a[2].x(1);
  };
  auto X2YY = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(2)*a[1].y(1)* a[2].y(1);
  };    

  auto X2XY = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(2)*a[1].x(1)* a[2].y(1);
  };
  auto X2YX = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(2)*a[1].y(1)* a[2].x(1);
  };
  auto Y2XX = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(2)*a[1].x(1)* a[2].x(1);
  };
  auto Y2YY = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(2)*a[1].y(1)* a[2].y(1);
  };    
  
  
/*  auto Y3YYY = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].y(1) * a[3].y(1);
  };    
  auto Y3XYY = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].y(1) * a[3].y(1);
  };      
  auto Y3YXY = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].x(1) * a[3].y(1);
  };      
  auto Y3YYX = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].y(1) * a[3].x(1);
  };      
  auto Y3YXX = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].x(1) * a[3].x(1);
  };      
  auto Y3XYX = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].y(1) * a[3].x(1);
  };      
  auto Y3XXY = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].x(1) * a[3].y(1);
  };      
  auto Y3XXX = [](const std::vector<Qn::QVector> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].x(1) * a[3].x(1);
  };      
  
  auto X3YYY = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].y(1) * a[3].y(1);
  };    
  auto X3XYY = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].y(1) * a[3].y(1);
  };      
  auto X3YXY = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].x(1) * a[3].y(1);
  };      
  auto X3YYX = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].y(1) * a[3].x(1);
  };      
  auto X3YXX = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].x(1) * a[3].x(1);
  };      
  auto X3XYX = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].y(1) * a[3].x(1);
  };      
  auto X3XXY = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].x(1) * a[3].y(1);
  };      
  auto X3XXX = [](const std::vector<Qn::QVector> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].x(1) * a[3].x(1);
  };  */  

  a.SetOutputFile("corr.root");
  
  a.AddQVectors("psd1, psd2, psd3, proton_pT, proton_y, pion_pT, pion_y, kaon_pT, kaon_y, track_r1, track_r2, psi");
  if (issim_)
      a.AddQVectors("mc_proton_pT, mc_proton_y, mc_pion_pT, mc_pion_y, mc_kaon_pT, mc_kaon_y,");
  
  a.AddEventVariable({"CentralityPsd", {0,5,10,15,25,35,45,60,80,100.} });
 
  const std::string sDet[] = { "psd1", "psd2", "psd3", "track_r1", "track_r2", "proton_pT", "proton_y", "pion_pT", "pion_y", "kaon_pT", "kaon_y"};
  const std::string sMc[] = { "mc_proton_pT", "mc_proton_y", "mc_pion_pT", "mc_pion_y", "mc_kaon_pT", "mc_kaon_y" };

  const std::string sTracks[] = { sDet[3], sDet[4], sDet[5], sDet[6], sDet[7], sDet[8], sDet[9], sDet[10] };
  const std::string sPsd[] = { sDet[0], sDet[1], sDet[2]};
  
  const std::string sPsdPsdName[] = { "psd1, psd2", "psd2, psd3", "psd3, psd1" };
  const std::string sPsdPsd[] = { "psd1_psd2", "psd2_psd3", "psd3_psd1", };
  const std::string sDetSim[] = { "psd1, psi", "psd2, psi", "psd3, psi"};

  const std::string sPsdPsdPsd = "psd1_psd2_psd3";
  const std::string sPsdPsdPsdName = "psd1, psd2, psd3";


  if (issim_)
  {
    for (ushort iDet=0; iDet<11; ++iDet)
    {
        a.AddCorrelation( sDet[iDet] + "_psi_XX", sDet[iDet] + ", psi", XX);
        a.AddCorrelation( sDet[iDet] + "_psi_YY", sDet[iDet] + ", psi", YY);
        a.AddCorrelation( sDet[iDet] + "_psi_XY", sDet[iDet] + ", psi", XY);
        a.AddCorrelation( sDet[iDet] + "_psi_YX", sDet[iDet] + ", psi", YX);
    }
    for (ushort iDet=0; iDet<6; ++iDet)
    {
        a.AddCorrelation( sMc[iDet] + "_psi_XX", sMc[iDet] + ", psi", XX);
        a.AddCorrelation( sMc[iDet] + "_psi_YY", sMc[iDet] + ", psi", YY);
        a.AddCorrelation( sMc[iDet] + "_psi_XY", sMc[iDet] + ", psi", XY);
        a.AddCorrelation( sMc[iDet] + "_psi_YX", sMc[iDet] + ", psi", YX);
    }  
 }
  for (ushort iPsd=0; iPsd<3; ++iPsd)
  for (ushort iTrack=0; iTrack<8; ++iTrack)
  {
    a.AddCorrelation( sTracks[iTrack] + "_" + sPsd[iPsd]+ "_XX", sTracks[iTrack] + ", " + sPsd[iPsd], XX);
    a.AddCorrelation( sTracks[iTrack] + "_" + sPsd[iPsd]+ "_YY", sTracks[iTrack] + ", " + sPsd[iPsd], YY);
    a.AddCorrelation( sTracks[iTrack] + "_" + sPsd[iPsd]+ "_XY", sTracks[iTrack] + ", " + sPsd[iPsd], XY);
    a.AddCorrelation( sTracks[iTrack] + "_" + sPsd[iPsd]+ "_YX", sTracks[iTrack] + ", " + sPsd[iPsd], YX);
  }  
  
  for (ushort iDet=0; iDet<3; ++iDet)
  {
    a.AddCorrelation(sPsdPsd[iDet] + "_XX", sPsdPsdName[iDet], XX);        
    a.AddCorrelation(sPsdPsd[iDet] + "_YY", sPsdPsdName[iDet], YY);        
    a.AddCorrelation(sPsdPsd[iDet] + "_XY", sPsdPsdName[iDet], XY);        
    a.AddCorrelation(sPsdPsd[iDet] + "_YX", sPsdPsdName[iDet], YX);        

    for (ushort iTrack=0; iTrack<8; ++iTrack)
    {
        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_X2XX", sTracks[iTrack] + ", " + sPsdPsdName[iDet], X2XX);        
        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_X2YY", sTracks[iTrack] + ", " + sPsdPsdName[iDet], X2YY);        
        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_Y2XY", sTracks[iTrack] + ", " + sPsdPsdName[iDet], Y2XY);        
        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_Y2YX", sTracks[iTrack] + ", " + sPsdPsdName[iDet], Y2YX);                

        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_Y2XX", sTracks[iTrack] + ", " + sPsdPsdName[iDet], Y2XX);        
        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_Y2YY", sTracks[iTrack] + ", " + sPsdPsdName[iDet], Y2YY);        
        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_X2XY", sTracks[iTrack] + ", " + sPsdPsdName[iDet], X2XY);        
        a.AddCorrelation(sTracks[iTrack] + "_"  + sPsdPsd[iDet] + "_X2YX", sTracks[iTrack] + ", " + sPsdPsdName[iDet], X2YX);                        
        
    }
}
//   for (ushort iTrack=0; iTrack<4; ++iTrack)
//   {
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3YYY", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3YYY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3XYY", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3XYY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3YXY", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3YXY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3YYX", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3YYX);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3YXX", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3YXX);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3XYX", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3XYX);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3XXY", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3XXY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_Y3XXX", sTracks[iTrack] + ", " + sPsdPsdPsdName, Y3XXX);        
// 
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3YYY", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3YYY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3XYY", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3XYY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3YXY", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3YXY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3YYX", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3YYX);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3YXX", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3YXX);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3XYX", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3XYX);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3XXY", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3XXY);        
//    a.AddCorrelation(sTracks[iTrack] + "_" + sPsdPsdPsd + "_X3XXX", sTracks[iTrack] + ", " + sPsdPsdPsdName, X3XXX);           
// }
    
}

void CorrelationTask::Run() {
  Qn::CorrelationManager a(reader_, in_tree_->GetEntries());
  in_tree_->LoadTree(0); // prevents weird TTree errors
  Configure(a);
  int events = 1;
  reader_->SetEntry(0); //  first entry needed to access configuration of DataContainers in the input file
  a.Initialize();
  in_tree_->LoadTree(0); // prevents weird TTree errors
  while (reader_->Next()) {
    events++;
    a.Process();
  }
  a.Finalize();
  std::cout << "number of analyzed events: " << events << std::endl;
}

//
//
//  auto multiply = [](Qn::Profile a, Qn::Profile b) {
//    return a*b;
//  };
//
//  auto divide = [](Qn::Profile a, Qn::Profile b) {
//    return a/b;
//  };
//
//  auto sqrt = [](Qn::Profile a) {
//    return a.Sqrt();
//  };
//
//  auto add = [](Qn::Profile a, Qn::Profile b) {
//    return a + b;
//  };
//
//  auto rvatpcvc = tpcva.Apply(vavc, multiply).Apply(tpcvc, divide).Map(sqrt);
//  auto v2tpcva = tpcva.Apply(rvatpcvc, divide);


std::unique_ptr<TChain> CorrelationTask::MakeChain(std::string filename, std::string treename) {
  std::unique_ptr<TChain> chain(new TChain(treename.data()));
  chain->AddFile(filename.data());
  
  std::cout << "Number of entries = " << chain->GetEntries() << std::endl;
  
//   std::ifstream in;
//   in.open(filename);
//   std::string line;
//   std::cout << "files in TChain:" << "\n";
//   while ((in >> line).good()) {
//     if (!line.empty()) {
//       chain->AddFile(line.data());
//       std::cout << line << std::endl;
//     }
//   }
  return chain;
}

