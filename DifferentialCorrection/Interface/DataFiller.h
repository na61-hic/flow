//
// Created by Lukas Kreis on 17.01.18.
//

#ifndef FLOW_DATAFILLER_H
#define FLOW_DATAFILLER_H

#include "DifferentialCorrection/Detector.h"

#include "DifferentialCorrection/Interface/VarManager.h"

#include "DataTreeEvent.h"
#include "DataTreeTrack.h"

#include "centrality/Getter.h"
#include "pid/Getter.h"


#define VAR VarManager

namespace Qn {
class DataFiller {
 public:
  using MapDetectors = std::map<std::string, Detector>;
  explicit DataFiller(DataTreeEvent *event) : event_(event) {}
  void FillEventInfo(const std::shared_ptr<VariableManager> &var_manager) {
    VAR::FillEventInfo(event_, var_manager->GetVariableContainer(), centr_);
  }
  void FillDetectors(MapDetectors &channel, MapDetectors  &tracking,
                     const std::shared_ptr<VariableManager> &var_manager) {
    for (auto &dp : channel) { dp.second.FillData(); }
    
    DataTreeTrack *track {nullptr};
    DataTreeMCTrack *mctrack {nullptr};

    for (int iTrack = 0; iTrack < event_->GetNVertexTracks(); iTrack++) {
        track = event_->GetVertexTrack(iTrack);
        if (track->GetVtxChi2() > 3) continue;
        VAR::FillTrackInfo(track, var_manager->GetVariableContainer(), event_, pid_, heff_);
      for (auto &dp : tracking) { dp.second.FillData(); }
    }
    for (int iTrack = 0; iTrack < event_->GetNMCTracks(); iTrack++) {
        mctrack = event_->GetMCTrack(iTrack);
        VAR::FillMCTrackInfo(mctrack, var_manager->GetVariableContainer());
      for (auto &dp : tracking) { dp.second.FillData(); }
    }
        
  }
  
  void SetCentralityGetter(Centrality::Getter *centr) { centr_ = centr; }; 
  void SetPidGetter(Pid::Getter *pid) { pid_ = pid; }; 
  void SetEff(std::array <TH2D, 3> &h) { heff_ = h; }

 private:
  DataTreeEvent *event_{nullptr};     
  Centrality::Getter *centr_{nullptr};     
  Pid::Getter *pid_{nullptr};     
  std::array <TH2D, 3> heff_;

};
}
#endif //FLOW_DATAFILLER_H
