
#ifndef VARMANAGER_H
#define VARMANAGER_H

#include "TH2.h"

#include "DataTreeEvent.h"
#include "DataTreeTrack.h"
#include "DataTreeMCTrack.h"

#include "DataTreeCuts.h"
#include "centrality/Getter.h"
#include "pid/Getter.h"

namespace Qn {

class VarManager {

 public:

enum Values {
    kCentralityPsd = 0,
    kCentralityTracks,
    kVertexX,
    kVertexY,
    kVertexZ,    
    kRunNumber,
    kImpactPar,
    kPsiRP,
    kPsdEnergyTotal,
    kPsdEnergy,
    kPsdPhi = kPsdEnergy+44,
    kNEventVars = kPsdPhi+44,                               // number of event variables  
    // Particle variables --------------------------------------
    // Common pair/track variables
    kRapidity = kNEventVars,
    kPt,
    kMCPt,
    kEta,
    kMCEta,
    kMCRapidity,
    kPhi,
    kMCPhi,
    kPid,
    kMCPid,
    kCharge,
    kMCCharge,
    kM2,
    kP,
    kMCP,
    kWeight,
    kChi2Vtx
  };
  
  static constexpr double ybeam{1.530965};
//   static constexpr double ybeam{2.079};
//   static constexpr double ybeam{1.6617};

  static void FillTrackInfo(const DataTreeTrack *track, double *values, const DataTreeEvent *event,  Pid::Getter *pid_getter, std::array <TH2D, 3> &heff) 
  {
    float y{-999};    
    int pid{-1};
    const float p{track->GetP()};
    float m2{-999.};
    values[kWeight] = 1.;

    const int tofid{track->GetTOFHitId()};
    if (tofid >= 0)
    {
        auto tof = event->GetTOFHit(tofid);
        m2 = tof->GetSquaredMass();
    }
    
    if (m2 != -999.)
    {
        if (pid_getter != nullptr)
            pid = pid_getter->GetPid(p, m2, 0.9) * track->GetCharge();
        else
        {
            std::cout << "Pid::Getter is invalid!" << std::endl;
            exit(0);
        }
    }
    if (pid == 2212 || pid == -2212)  
    {
        const float pz = track->GetPz();
        const float e=sqrt( p*p + 0.938*0.938 );
        y = 0.5*log( (e+pz)/(e-pz) ) - ybeam ;
        {
            const float eff = heff[0].GetBinContent( heff[0].FindBin(y, track->GetPt()) ) ;
            values[kWeight] = eff > 0.05 ? 1/eff : 1. ;
        }
    }
    else if (pid == 321 || pid == -321)  
    {
        const float pz = track->GetPz();
        const float e=sqrt( p*p + 0.5*0.5 );
        y = 0.5*log( (e+pz)/(e-pz) ) - ybeam ;
        {
            const float eff = heff[1].GetBinContent( heff[1].FindBin(y, track->GetPt()) ) ;
            values[kWeight] = eff > 0.05 ? 1/eff : 1. ;
        }
    }    
    else 
    {
        y = track->GetRapidity() - ybeam;
        if ( (pid == 211 || pid == -211) )
        {
            const float eff = heff[2].GetBinContent( heff[2].FindBin(y, track->GetPt()) ) ;
            values[kWeight] = eff > 0.05 ? 1/eff : 1. ;
        }
    }
    values[kM2] = m2;
    values[kP] = p;
    values[kPt] = track->GetPt();
    values[kRapidity] = y;
    values[kPhi] = track->GetPhi();
    values[kPid] = pid;
    values[kCharge] = track->GetCharge();
    values[kEta] = track->GetEta();
    values[kChi2Vtx] = track->GetVtxChi2();
    
    values[kMCPt] = -999;
    values[kMCP] = -999;    
    values[kMCRapidity] =  -999;
    values[kMCPhi] =  -999;
    values[kMCPid] =  -999;
    values[kMCCharge] =  -999;
    values[kMCEta] =  -999;
    
  }  
  
  static void FillMCTrackInfo(const DataTreeMCTrack *track, double *values) 
  {
    values[kMCPt] = track->GetPt();
    values[kMCP] = track->GetP();    
    values[kMCRapidity] = track->GetRapidity() - ybeam;
    values[kMCPhi] = track->GetPhi();
    values[kMCPid] = track->GetPdgId();
    values[kMCCharge] = track->GetCharge();
    values[kMCEta] = track->GetEta();
    
    values[kPt] = -999;
    values[kP] = -999;    
    values[kRapidity] =  -999;
    values[kPhi] =  -999;
    values[kPid] =  -999;
    values[kCharge] =  -999;
    values[kEta] =  -999;    
  }
  
  static void FillEventInfo(const DataTreeEvent *event, double *values, const Centrality::Getter *centr) 
  {
//     std::cout << "FillEventInfo" << std::endl;
    
    values[kPsdEnergyTotal] = event->GetPSDEnergy();
    if (centr != nullptr)
        values[kCentralityPsd] = centr->GetCentrality(event->GetPSDEnergy());
    else
    {
        std::cout << "Centrality::Getter is invalid!" << std::endl;
        exit(0);
    }
    
    values[kRunNumber] = event->GetRunId();
    values[kPsiRP] = event->GetRPAngle();
    
    values[kVertexX] = event->GetVertexPositionComponent(0);
    values[kVertexY] = event->GetVertexPositionComponent(1);
    values[kVertexZ] = event->GetVertexPositionComponent(2);
    
    DataTreePSDModule *psd{nullptr};
    
    for (u_short ich = 0; ich < event->GetNPSDModules(); ++ich) 
    {
  
      psd = event->GetPSDModule( ich );  // modules numbering starts with 1

      if (psd == nullptr) 
      {
          std::cout << "NO PSD MODULE # " << ich << " IN DATATREE" << std::endl; 
          continue; 
      }

      const double x = psd->GetPositionComponent(0);
      const double y = psd->GetPositionComponent(1);
      const double phi = TMath::ATan2(y, x);
      const double weight = psd->GetEnergy();

//       std::cout << "ich = " << ich << " e = " << weight << " phi = " << phi << std::endl;

      values[kPsdEnergy+ich] = weight;
      values[kPsdPhi+ich] = phi;
    }
  }
  
};
}

#endif


