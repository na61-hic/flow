#include <iostream>
#include <vector>
#include <random>
#include <array>
#include <iomanip>
#include <chrono>

#include <TSystem.h>
#include <TROOT.h>
#include "TTree.h"
#include "TTreeReader.h"
#include "TH2.h"

#include "Base/DataContainer.h"
#include "Correlation/Correlation.h"

#include "CorrelationTask.h"
#include "CorrectionTask.h"

// #include "qa/DataTreeQAConfig.h"
#include "DataTreeCuts.h"


int main(int argc, char **argv) {
  auto start = std::chrono::system_clock::now();
//   ROOT::EnableImplicitMT(20);
    
  const bool issim = true;
  const std::string setup = "cbm";
  
  if (strcmp(argv[3], "correct")==0) {
    Qn::CorrectionTask task(argv[1], argv[2], "fDTEvent");  
    task.SetSetup(setup);
    task.SetIsSim(issim);
    task.Run();
  }
  if (strcmp(argv[3], "analysis")==0) {
    CorrelationTask  st(argv[1], "tree");
    st.SetSetup(setup);
    st.SetIsSim(issim);
    st.Run();
  }
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::cout << "elapsed time: " << elapsed_seconds.count() << " s\n";
  return 0;
}
