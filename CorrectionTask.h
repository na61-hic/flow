//
// Created by Lukas Kreis on 29.06.17.
//

#ifndef FLOW_QNCorrectionTask_H
#define FLOW_QNCorrectionTask_H

#include <vector>
#include <array>
#include <random>
#include "QnCorrections/QnCorrectionsProfile3DCorrelations.h"
#include "QnCorrections/QnCorrectionsProfileCorrelationComponents.h"
#include "QnCorrections/QnCorrectionsDetectorConfigurationChannels.h"
#include "QnCorrections/QnCorrectionsDetectorConfigurationBase.h"
#include <QnCorrections/QnCorrectionsDetectorConfigurationTracks.h>
#include <QnCorrections/QnCorrectionsQnVectorRecentering.h>
#include <QnCorrections/QnCorrectionsQnVectorTwistAndRescale.h>
#include <QnCorrections/QnCorrectionsCutSetBit.h>
#include <QnCorrections/QnCorrectionsCutWithin.h>
#include <QnCorrections/QnCorrectionsInputGainEqualization.h>
#include <QnCorrections/QnCorrectionsQnVectorAlignment.h>
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TTreeReader.h"
#include "TH2.h"

#include "QnCorrections/QnCorrectionsManager.h"
#include "Base/DataContainer.h"
#include "DifferentialCorrection/EventInfo.h"
#include "DifferentialCorrection/CorrectionManager.h"

#include "DifferentialCorrection/Interface/VarManager.h"

#include "DataTreeEvent.h"
#include "DataTreeCuts.h"

#include "centrality/Getter.h"
#include "pid/Getter.h"


#define VAR VarManager

namespace Qn {
/**
 * Qn vector analysis CorrectionTask. It is to be configured by the user.
 * @brief CorrectionTask for analysing qn vectors
 */
class CorrectionTask {
 public:
  CorrectionTask() = default;
  CorrectionTask(std::string filelist, std::string incalib, std::string treename);
  CorrectionTask(std::array<std::shared_ptr<TFile>, 4> files);
  ~CorrectionTask() = default;
  void Run();

 void SetIsSim(bool is=true) { issim_ = is; }
 void SetSetup(std::string setup) { setup_ = setup; }
  
 std::string GetSetup() const { return setup_; }
 bool GetIsSim() const { return issim_; }

 void SetCuts (cuts::DataTreeCuts* cuts) { cuts_ = cuts; }
 cuts::DataTreeCuts* GetCuts () const { return cuts_; }

 void SetCentrCuts (cuts::DataTreeCuts* cuts) { centr_cuts_ = cuts; }
 cuts::DataTreeCuts* GetCentrCuts () const { return centr_cuts_; }

 void SetEff(std::array <TH2D, 3> &h) { heff_ = h; }
 
 
private:
  /**
   * Initializes CorrectionTask;
   */
  virtual void Initialize();
  /**
   * Processes one event;
   */
  virtual void Process();
  /**
   * Finalizes CorrectionTask. Called after processing is done.
   */
  virtual void Finalize();
  /**
   * Make TChain from file list
   * @param filename name of file containing paths to root files containing the input trees
   * @return Pointer to the TChain
   */
 virtual std::unique_ptr<TChain> MakeChain(std::string filename, std::string treename);
 
protected:
  std::unique_ptr<TTree> in_tree_;
  std::shared_ptr<TFile> out_file_;
  std::shared_ptr<TFile> in_calibration_file_;
  std::shared_ptr<TFile> out_calibration_file_;
  std::unique_ptr<TTree> out_tree_;
  std::unique_ptr<TTree> out_tree_raw;
  
//   TofPidGetter *tof_;
  
  Qn::CorrectionManager manager_;
  bool write_tree_;
  
  DataTreeEvent *event_;
//   CentralityManager *centr_;

  bool issim_{true};
  std::string setup_{""};
  cuts::DataTreeCuts* cuts_;
  cuts::DataTreeCuts* centr_cuts_;

  std::array <TH2D, 3> heff_;

  Centrality::Getter *centr_getter_{nullptr};     
  Pid::Getter *pid_getter_{nullptr};     
  
};
}
#endif //FLOW_QNTASK_H
