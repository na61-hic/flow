//
// Created by Lukas Kreis on 29.06.17.
//

#include <iostream>
#include <QnCorrections/QnCorrectionsLog.h>
#include <random>
#include <THnSparse.h>
#include "CorrectionTask.h"

#include "CorrectionConfig.h"

namespace Qn {
    
CorrectionTask::CorrectionTask(std::string filelist, std::string incalib, std::string treename) :
in_tree_(this->MakeChain(filelist, "DataTree")),
out_file_(new TFile("output.root", "RECREATE")),
in_calibration_file_(new TFile(incalib.data(), "READ")),
out_calibration_file_(new TFile("qn.root", "RECREATE")),
out_tree_(nullptr),
out_tree_raw(nullptr),
manager_(),
write_tree_(true), 
event_(nullptr)
{
    event_ = new DataTreeEvent();
    in_tree_->SetBranchAddress(treename.c_str(), &event_);
    out_file_->cd();
    
    std::unique_ptr<TTree> treeraw(new TTree("datatree", "datatree"));
    std::unique_ptr<TTree> tree(new TTree("tree", "tree"));
    out_tree_ = std::move(tree);
    out_tree_raw = std::move(treeraw);
    
//     std::unique_ptr<TFile> centr_file {TFile::Open("/home/vklochkov/Data/cbm/urqmd/au10au/centrality.root", "read")};
    std::unique_ptr<TFile> centr_file {TFile::Open("/lustre/nyx/cbm/users/klochkov/cbm/auau/10agev/urqmd/centrality/centrality.root", "read")};
    centr_getter_ = (Centrality::Getter*) centr_file->Get("centr_getter_1d");
    
//     std::unique_ptr<TFile> pid_file {TFile::Open("/home/vklochkov/Data/cbm/urqmd/au10au/pid.root", "read")};
    std::unique_ptr<TFile> pid_file {TFile::Open("/lustre/nyx/cbm/users/klochkov/cbm/auau/10agev/urqmd/pid/pid_getter.root", "read")};
    pid_getter_ = (Pid::Getter*) pid_file->Get("pid_getter");

    std::unique_ptr<TFile> eff_file {TFile::Open("/lustre/nyx/cbm/users/klochkov/cbm/auau/10agev/urqmd/qa/cbm_auau_10agev_config/eff.root", "read")};
    heff_[0] = *( (TH2D*) eff_file->Get("p_eff") );
    heff_[1] = *( (TH2D*) eff_file->Get("k_eff") );
    heff_[2] = *( (TH2D*) eff_file->Get("pi_eff"));
}

void CorrectionTask::Run() {
    Initialize();
    QnCorrectionsSetTracingLevel(kError);
    std::cout << "Processing..." << std::endl;
    int i = 0;
    while (i < in_tree_->GetEntries() ) {
        in_tree_->GetEntry(i);
        
        //     if (cuts_ != nullptr && !cuts_->IsGoodEvent( *event_ ) )  { ++i;  continue; }
        
        Process();
        ++i;
    }
    std::cout << i << std::endl;
    Finalize();
}

void CorrectionTask::Initialize() {
    using Axes = std::vector<Qn::Axis>;
    
    Axis etaaxis("Eta", 20, 1.4, 5.0);
    Axis yaxis("Rapidity", 15, -0.6, 1.5);
    Axis ptaxis("Pt", { 0., .2, .4, .6, .8, 1., 1.2, 1.4, 1.8, 2.2, 2.6, 3.0 } );

    Axis mc_yaxis("MCRapidity", 15, -0.6, 1.5);
    Axis mc_ptaxis("MCPt", { 0., .2, .4, .6, .8, 1., 1.2, 1.4, 1.8, 2.2, 2.6, 3.0 } );

    Axes tpcaxes = {ptaxis, etaaxis};
    
    auto confTracks = [](QnCorrectionsDetectorConfigurationBase *config) {
        config->SetQVectorNormalizationMethod(QnCorrectionsQnVector::QVNORM_QoverM);
        config->AddCorrectionOnQnVector(new QnCorrectionsQnVectorRecentering());
        auto rescale = new QnCorrectionsQnVectorTwistAndRescale();
        rescale->SetApplyTwist(true);
        rescale->SetApplyRescale(true);
        
        rescale->SetTwistAndRescaleMethod(QnCorrectionsQnVectorTwistAndRescale::TWRESCALE_doubleHarmonic);
        config->AddCorrectionOnQnVector(rescale);
    };

    auto confMCTracks = [](QnCorrectionsDetectorConfigurationBase *config) {
        config->SetQVectorNormalizationMethod(QnCorrectionsQnVector::QVNORM_QoverM);
    };
    
    auto confMC = [](QnCorrectionsDetectorConfigurationBase *config) {
        config->SetQVectorNormalizationMethod(QnCorrectionsQnVector::QVNORM_QoverM);
        bool channels {kTRUE};
        int channelGroups {0};
        ((QnCorrectionsDetectorConfigurationChannels *) config)->SetChannelsScheme(&channels, &channelGroups, nullptr);
    };
    
    manager_.AddVariable("CentralityPsd", VAR::Values::kCentralityPsd, 1);
    manager_.AddVariable("RunNumber", VAR::Values::kRunNumber, 1);
    
    manager_.AddVariable("VertexX", VAR::Values::kVertexX, 1);
    manager_.AddVariable("VertexY", VAR::Values::kVertexY, 1);
    manager_.AddVariable("VertexZ", VAR::Values::kVertexZ, 1);
    manager_.AddVariable("PsdEnergyTotal", VAR::Values::kPsdEnergyTotal, 1);

    manager_.AddVariable("MCP", VAR::Values::kMCP, 1);
    manager_.AddVariable("MCPid", VAR::Values::kMCPid, 1);
    manager_.AddVariable("MCPt", VAR::Values::kMCPt, 1);
    manager_.AddVariable("MCEta", VAR::Values::kMCEta, 1);
    manager_.AddVariable("MCRapidity", VAR::Values::kMCRapidity, 1);
    manager_.AddVariable("MCCharge", VAR::Values::kMCCharge, 1);
    manager_.AddVariable("MCPhi", VAR::Values::kMCPhi, 1);
    
    manager_.AddVariable("M2", VAR::Values::kM2, 1);
    manager_.AddVariable("P", VAR::Values::kP, 1);
    manager_.AddVariable("Pid", VAR::Values::kPid, 1);
    manager_.AddVariable("Pt", VAR::Values::kPt, 1);
    manager_.AddVariable("Eta", VAR::Values::kEta, 1);
    manager_.AddVariable("Rapidity", VAR::Values::kRapidity, 1);
    manager_.AddVariable("Charge", VAR::Values::kCharge, 1);
    manager_.AddVariable("Phi", VAR::Values::kPhi, 1);
    manager_.AddVariable("Weight", VAR::Values::kWeight, 1); // 1/eff
    manager_.AddVariable("Chi2Vtx", VAR::Values::kChi2Vtx, 1);

    manager_.AddVariable("PsdEnergy", VAR::Values::kPsdEnergy, 44);
    manager_.AddVariable("PsdPhi", VAR::Values::kPsdPhi, 44);
    manager_.AddVariable("PsiRP", VAR::Values::kPsiRP, 1);


    constexpr float y_cut_low = 0.;
    constexpr float y_cut_high = 1.2;
    constexpr float pT_cut_low = 0.05;
    constexpr float pT_cut_high = 2.;
    
// ***********  proton  ***********
    const std::string p_name_pT = "proton_pT";
    manager_.AddDetector(p_name_pT, DetectorType::TRACK, "Phi", "Weight", {ptaxis});
    auto p_cut_pT = [](double &y, double &pid) { 
        return (y > y_cut_low && y < y_cut_high) && (pid == 2212); 
    };
    manager_.AddCut(p_name_pT, {"Rapidity", "Pid"}, p_cut_pT);
    manager_.SetCorrectionSteps(p_name_pT, confTracks);
    
    const std::string p_name_y = "proton_y";
    manager_.AddDetector(p_name_y, DetectorType::TRACK, "Phi", "Weight", {yaxis});
    auto p_cut_y = [](double &pT, double &pid) { 
        return (pT > pT_cut_low && pT < pT_cut_high) && (pid == 2212); 
    };
    manager_.AddCut(p_name_y, {"Pt", "Pid"}, p_cut_y);
    manager_.SetCorrectionSteps(p_name_y, confTracks);
            
    //MC
    const std::string mc_p_name_pT = "mc_proton_pT";
    manager_.AddDetector(mc_p_name_pT, DetectorType::TRACK, "MCPhi", "Ones", {mc_ptaxis});
    auto mc_p_cut_pT = [](double &y, double &pid) { 
        return (y > y_cut_low && y < y_cut_high) && (pid == 2212); 
    };
    manager_.AddCut(mc_p_name_pT, {"MCRapidity", "MCPid"}, mc_p_cut_pT);
    manager_.SetCorrectionSteps(mc_p_name_pT, confMCTracks);
    
    const std::string mc_p_name_y = "mc_proton_y";
    manager_.AddDetector(mc_p_name_y, DetectorType::TRACK, "MCPhi", "Ones", {mc_yaxis});
    auto mc_p_cut_y = [](double &pT, double &pid) { 
        return (pT > pT_cut_low && pT < pT_cut_high) && (pid == 2212); 
    };
    manager_.AddCut(mc_p_name_y, {"MCPt", "MCPid"}, mc_p_cut_y);
    manager_.SetCorrectionSteps(mc_p_name_y, confMCTracks);    
    
// *********************************

// ***********  pion  ***********
    const std::string pi_name_pT = "pion_pT";
    manager_.AddDetector(pi_name_pT, DetectorType::TRACK, "Phi", "Weight", {ptaxis});
    auto pi_cut_pT = [](double &y, double &pid) { 
        return (y > y_cut_low && y < y_cut_high) && (pid == 211); 
    };
    manager_.AddCut(pi_name_pT, {"Rapidity", "Pid"}, pi_cut_pT);
    manager_.SetCorrectionSteps(pi_name_pT, confTracks);
    
    const std::string pi_name_y = "pion_y";
    manager_.AddDetector(pi_name_y, DetectorType::TRACK, "Phi", "Weight", {yaxis});
    auto pi_cut_y = [](double &pT, double &pid) { 
        return (pT > pT_cut_low && pT < pT_cut_high) && (pid == 211); 
    };
    manager_.AddCut(pi_name_y, {"Pt", "Pid"}, pi_cut_y);
    manager_.SetCorrectionSteps(pi_name_y, confTracks);
    
    //MC
    const std::string mc_pi_name_pT = "mc_pion_pT";
    manager_.AddDetector(mc_pi_name_pT, DetectorType::TRACK, "MCPhi", "Ones", {mc_ptaxis});
    auto mc_pi_cut_pT = [](double &y, double &pid) { 
        return (y > y_cut_low && y < y_cut_high) && (pid == 211); 
    };
    manager_.AddCut(mc_pi_name_pT, {"MCRapidity", "MCPid"}, mc_pi_cut_pT);
    manager_.SetCorrectionSteps(mc_pi_name_pT, confMCTracks);
    
    const std::string mc_pi_name_y = "mc_pion_y";
    manager_.AddDetector(mc_pi_name_y, DetectorType::TRACK, "MCPhi", "Ones", {mc_yaxis});
    auto mc_pi_cut_y = [](double &pT, double &pid) { 
        return (pT > pT_cut_low && pT < pT_cut_high) && (pid == 211); 
    };
    manager_.AddCut(mc_pi_name_y, {"MCPt", "MCPid"}, mc_pi_cut_y);
    manager_.SetCorrectionSteps(mc_pi_name_y, confMCTracks);    
    
    
    
// *********************************

// ***********  kaon  ***********
    const std::string k_name_pT = "kaon_pT";
    manager_.AddDetector(k_name_pT, DetectorType::TRACK, "Phi", "Weight", {ptaxis});
    auto k_cut_pT = [](double &y, double &pid) { 
        return (y > y_cut_low && y < y_cut_high) && (pid == 321); 
    };
    manager_.AddCut(k_name_pT, {"Rapidity", "Pid"}, k_cut_pT);
    manager_.SetCorrectionSteps(k_name_pT, confTracks);
    
    const std::string k_name_y = "kaon_y";
    manager_.AddDetector(k_name_y, DetectorType::TRACK, "Phi", "Weight", {yaxis});
    auto k_cut_y = [](double &pT, double &pid) { 
        return (pT > pT_cut_low && pT < pT_cut_high) && (pid == 321); 
    };
    manager_.AddCut(k_name_y, {"Pt", "Pid"}, k_cut_y);
    manager_.SetCorrectionSteps(k_name_y, confTracks);
    
    //MC
    const std::string mc_k_name_pT = "mc_kaon_pT";
    manager_.AddDetector(mc_k_name_pT, DetectorType::TRACK, "MCPhi", "Ones", {mc_ptaxis});
    auto mc_k_cut_pT = [](double &y, double &pid) { 
        return (y > y_cut_low && y < y_cut_high) && (pid == 321); 
    };
    manager_.AddCut(mc_k_name_pT, {"MCRapidity", "MCPid"}, mc_k_cut_pT);
    manager_.SetCorrectionSteps(mc_k_name_pT, confMCTracks);
    
    const std::string mc_k_name_y = "mc_kaon_y";
    manager_.AddDetector(mc_k_name_y, DetectorType::TRACK, "MCPhi", "Ones", {mc_yaxis});
    auto mc_k_cut_y = [](double &pT, double &pid) { 
        return (pT > pT_cut_low && pT < pT_cut_high) && (pid == 321); 
    };
    manager_.AddCut(mc_k_name_y, {"MCPt", "MCPid"}, mc_k_cut_y);
    manager_.SetCorrectionSteps(mc_k_name_y, confMCTracks);    
    
// *********************************
    manager_.AddDetector("track_r1", DetectorType::TRACK, "Phi", "Ones" );
    manager_.AddCut("track_r1", {"Pt", "Rapidity", "Pid"}, [](double &pT, double &y, double &pid) 
                                                          { return (pT > pT_cut_low && pT < pT_cut_high) && (y > -0.5 && y < 1.2) && (abs(pid)==211) ;});
    
    manager_.AddDetector("track_r2", DetectorType::TRACK, "Phi", "Ones" );
    manager_.AddCut("track_r2", {"Pt", "Rapidity"}, [](double &pT, double &y) { return (pT > pT_cut_low && pT < pT_cut_high) && (y > 0.8 && y < 1.8); });

    manager_.AddDetector("psd1", DetectorType::CHANNEL, "PsdPhi", "PsdEnergy");
    manager_.AddDetector("psd2", DetectorType::CHANNEL, "PsdPhi", "PsdEnergy");
    manager_.AddDetector("psd3", DetectorType::CHANNEL, "PsdPhi", "PsdEnergy");
    manager_.AddDetector("psi", DetectorType::CHANNEL, "PsiRP", "Ones");
    
    manager_.SetCorrectionSteps("track_r1", confTracks);
    manager_.SetCorrectionSteps("track_r2", confTracks);
    manager_.SetCorrectionSteps("psd1", Qn::CorrectionConfig::confPSD1 );
    manager_.SetCorrectionSteps("psd2", Qn::CorrectionConfig::confPSD2 );
    manager_.SetCorrectionSteps("psd3", Qn::CorrectionConfig::confPSD3 );
    manager_.SetCorrectionSteps("psi", confMC);
    
    manager_.SetEventVariable("CentralityPsd");
    manager_.AddCorrectionAxis({"CentralityPsd", {0,5,10,15,25,35,45,60,80,100.} });
    
    Qn::CorrectionConfig::AddQAHisto(manager_);
    
    manager_.SetTree(std::move(out_tree_));
    manager_.Initialize(in_calibration_file_);
}

void CorrectionTask::Process() {
    manager_.Reset();
    
    DataFiller df(event_);
    df.SetCentralityGetter(centr_getter_);
    df.SetPidGetter(pid_getter_);
    df.SetEff(heff_);
    
    manager_.Process(df);
}

void CorrectionTask::Finalize() {
    manager_.Finalize();
    manager_.SaveHistograms(out_calibration_file_);
    manager_.SaveTree(out_file_);
}


std::unique_ptr<TChain> CorrectionTask::MakeChain(std::string filename, std::string treename) {
    std::unique_ptr<TChain> chain(new TChain(treename.data()));
    std::ifstream in;
    in.open(filename);
    std::string line;
    std::cout << "Adding files to chain:" << std::endl;
    while ((in >> line).good()) {
        if (!line.empty()) {
            chain->AddFile(line.data());
            std::cout << line << std::endl;
        }
    }
    std::cout << "Number of entries = " << chain->GetEntries() << std::endl;
    return chain;
}
    
}
