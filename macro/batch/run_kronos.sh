#!/bin/bash
#SBATCH -J Flow
#SBATCH -o out/%j.out.log
#SBATCH -e error/%j.err.log
#SBATCH --time=01:00:00
#SBATCH --array=0-99

INDEX=$(($SLURM_ARRAY_TASK_ID))
INDEX=$(printf "%03d" "$INDEX")

EXE_DIR=/lustre/nyx/cbm/users/klochkov/soft/flow/install

OUT_DIR=/lustre/nyx/cbm/users/klochkov/cbm/auau/10agev/urqmd/flow/new_final/
FILELIST_DIR=/lustre/nyx/cbm/users/klochkov/mc/cbmsim/urqmd/auau/10agev/mbias/sis100_electron_psd44/TGeant3/filelist/

module use /cvmfs/it.gsi.de/modules && module load gcc/6.4.0
source /lustre/nyx/cbm/users/klochkov/soft/root/root6/v6-12-06-cxx14/install/bin/thisroot.sh
source /lustre/nyx/cbm/users/klochkov/soft/set_frameworks.sh

mkdir -p $OUT_DIR/$INDEX
cd $OUT_DIR/$INDEX
cp $EXE_DIR/main ./

FILELIST=$FILELIST_DIR/filelist_$INDEX
#     split -n l/10 -d -a 3 filelist.txt filelist_

    for i in `seq 0 2`;
    do
        INFILE=qn_step_$(($i-1)).root
        echo $(ls -lrt)
        ./main $FILELIST $INFILE correct  &> log_correction_$i.txt
        mv qn.root qn_step_$i.root
        mv output.root output_step_$i.root
    done   
        
    for CORR_STEP in `seq 0 2`;
    do
        IN_FILE=output_step_${CORR_STEP}.root
        OUT_FILE=qncorr_${CORR_STEP}.root

        ./main $IN_FILE dfvgdf analysis &> log_correlation_${CORR_STEP}.txt
        mv corr.root $OUT_FILE
    done

rm main

echo "DONE"
