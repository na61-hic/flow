#ifndef FLOW_QNCorrectionConfig_H
#define FLOW_QNCorrectionConfig_H

#include "DifferentialCorrection/CorrectionManager.h"


namespace Qn::CorrectionConfig {


const int nModules=44;

const std::vector <std::string> track_names = { "proton_pT", "proton_y", "pion_pT", "pion_y", "kaon_pT", "kaon_y", "track_r1", "track_r2" };

const std::vector <std::string> mc_track_names = { "mc_proton_pT", "mc_proton_y", "mc_pion_pT", "mc_pion_y", "mc_kaon_pT", "mc_kaon_y"};
                                                
                                                
                                                
const std::vector <std::string> psd_names = {"psd1", "psd2", "psd3"};

//na61
// const std::vector<std::vector<int>> psdSubevents = { 
//     {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16},
//     {17,18,19,20,21,22,23,24,25,26,27,28},
//     {29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44} };

//cbm-44
const std::vector<std::vector<int>> psdSubevents = { 
  { 19, 20, 25, 26},
  { 12, 13, 14, 15, 18, 21, 24, 27, 30, 31, 32, 33},
  { 1,2,3,4,5,6,7,8,9,10,11,16,17,22,23,28,29,34,35,36,37,38,39,40,41,42,43,44} };

// //cbm-44 (old)
// const std::vector<std::vector<int>> psdSubevents = { 
//   {18, 19, 26, 27},
//   {9, 10, 11, 12, 17, 20, 33, 34, 35, 36, 25, 28},
//   {1,2,3,4,5,6,7,8,13,14,15,16,21,22,23,24,29,30,31,32,37,38,39,40,41,42,43,44} };
    
  
void AddQAHisto(Qn::CorrectionManager &manager)
{
    
    for (auto sub_track : track_names )
    {
        manager.AddHisto1D(sub_track, {{"Phi", 500, -4, 4}}, "Ones");
        manager.AddHisto1D(sub_track, {{"Pt", 500, 0, 3}}, "Ones");
        manager.AddHisto1D(sub_track, {{"Rapidity", 500, -1, 3}}, "Ones");
        manager.AddHisto1D(sub_track, {{"Eta", 500, -2, 7}}, "Ones");
        manager.AddHisto1D(sub_track, {{"Pid", 1000, -2500, 2500}}, "Ones");
        manager.AddHisto1D(sub_track, {{"M2", 500, -0.5, 1.2}}, "Ones");
        manager.AddHisto1D(sub_track, {{"P", 500, 0, 10}}, "Ones");
        manager.AddHisto1D(sub_track, {{"Chi2Vtx", 100, 0, 5}}, "Ones");
        
        manager.AddHisto2D(sub_track, {{"P", 500, 0, 10}, {"M2", 500, -0.5, 1.2}});
        manager.AddHisto2D(sub_track, {{"Phi", 500, -4, 4}, {"Rapidity", 500, -1, 3}});
        manager.AddHisto2D(sub_track, {{"Phi", 500, -4, 4}, {"Pt", 500, 0, 3}});
        manager.AddHisto2D(sub_track, {{"Phi", 500, -4, 4}, {"Eta", 500, -2, 7}});
        manager.AddHisto2D(sub_track, {{"Rapidity", 500, -1, 3}, {"Pt", 500, 0, 3}});
        manager.AddHisto2D(sub_track, {{"Eta", 500, -2, 7}, {"Pt", 500, 0, 3}});
    }
    
    
    for (auto sub_track : mc_track_names )
    {
        manager.AddHisto1D(sub_track, {{"MCPhi", 500, -4, 4}}, "Ones");
        manager.AddHisto1D(sub_track, {{"MCPt", 500, 0, 3}}, "Ones");
        manager.AddHisto1D(sub_track, {{"MCRapidity", 500, -1, 3}}, "Ones");
        manager.AddHisto1D(sub_track, {{"MCEta", 500, -2, 7}}, "Ones");
        manager.AddHisto1D(sub_track, {{"MCPid", 1000, -2500, 2500}}, "Ones");
        manager.AddHisto1D(sub_track, {{"MCP", 500, 0, 10}}, "Ones");

        manager.AddHisto2D(sub_track, {{"MCPhi", 500, -4, 4}, {"MCRapidity", 500, -1, 3}});
        manager.AddHisto2D(sub_track, {{"MCPhi", 500, -4, 4}, {"MCPt", 500, 0, 3}});
        manager.AddHisto2D(sub_track, {{"MCPhi", 500, -4, 4}, {"MCEta", 500, -2, 7}});
        manager.AddHisto2D(sub_track, {{"MCRapidity", 500, -1, 3}, {"MCPt", 500, 0, 3}});
        manager.AddHisto2D(sub_track, {{"MCEta", 500, -2, 7}, {"MCPt", 500, 0, 3}});
    }
        
    
    
    
    manager.AddHisto1D("psd1", {{"PsdEnergy", 500, 0, 10}}, "Ones");
    manager.AddHisto1D("psd1", {{"PsdPhi", 500, -4, 4}}, "Ones");
    manager.AddHisto1D("psd2", {{"PsdEnergy", 500, 0, 10}}, "Ones");
    manager.AddHisto1D("psd2", {{"PsdPhi", 500, -4, 4}}, "Ones");
    manager.AddHisto1D("psd3", {{"PsdEnergy", 500, 0, 10}}, "Ones");
    manager.AddHisto1D("psd3", {{"PsdPhi", 500, -4, 4}}, "Ones");
    
    manager.AddHisto1D("psi", {{"PsiRP", 500, -4, 4}}, "Ones");

    manager.AddEventHisto1D({{"CentralityPsd", 20, 0, 100}});
    manager.AddEventHisto1D({{"VertexX", 500, -1, 1}});
    manager.AddEventHisto1D({{"VertexY", 500, -1, 1}});
    manager.AddEventHisto1D({{"VertexZ", 500, -1, 1}});
    manager.AddEventHisto2D({{"VertexX", 500, -1, 1}, {"VertexY", 500, -1, 1}});    
    manager.AddEventHisto2D({{"CentralityPsd", 20, 0, 100}, {"PsdEnergyTotal", 40, 0, 50}});    
}


  auto confPSD1 = [](QnCorrectionsDetectorConfigurationBase *config) {
    
    const int iPsd = 0;
      
    config->SetQVectorNormalizationMethod(QnCorrectionsQnVector::QVNORM_QoverM);
    auto recenter = new QnCorrectionsQnVectorRecentering();
    recenter->SetApplyWidthEqualization(false);
    config->AddCorrectionOnQnVector(recenter);
    auto *channels = new bool[nModules];
    for (int ich = 0; ich < nModules; ich++) channels[ich] = kFALSE;
    for (int iModule : psdSubevents.at(iPsd)) channels[iModule-1] = kTRUE;

    auto *channelGroups = new int[nModules];
    for (int ich = 0; ich < nModules; ich++) channelGroups[ich] = 0;

    ((QnCorrectionsDetectorConfigurationChannels *) config)->SetChannelsScheme(channels, channelGroups, nullptr);
  };

  auto confPSD2 = [](QnCorrectionsDetectorConfigurationBase *config) {
    
    const int iPsd = 1;
      
    config->SetQVectorNormalizationMethod(QnCorrectionsQnVector::QVNORM_QoverM);
    auto recenter = new QnCorrectionsQnVectorRecentering();
    recenter->SetApplyWidthEqualization(false);
    config->AddCorrectionOnQnVector(recenter);
    auto *channels = new bool[nModules];
    for (int ich = 0; ich < nModules; ich++) channels[ich] = kFALSE;
    for (int iModule : psdSubevents.at(iPsd)) channels[iModule-1] = kTRUE;

    auto *channelGroups = new int[nModules];
    for (int ich = 0; ich < nModules; ich++) channelGroups[ich] = 0;

    ((QnCorrectionsDetectorConfigurationChannels *) config)->SetChannelsScheme(channels, channelGroups, nullptr);
  };
  
    auto confPSD3 = [](QnCorrectionsDetectorConfigurationBase *config) {
    
    const int iPsd = 2;
      
    config->SetQVectorNormalizationMethod(QnCorrectionsQnVector::QVNORM_QoverM);
    auto recenter = new QnCorrectionsQnVectorRecentering();
    recenter->SetApplyWidthEqualization(false);
    config->AddCorrectionOnQnVector(recenter);
    auto *channels = new bool[nModules];
    for (int ich = 0; ich < nModules; ich++) channels[ich] = kFALSE;
    for (int iModule : psdSubevents.at(iPsd)) channels[iModule-1] = kTRUE;

    auto *channelGroups = new int[nModules];
    for (int ich = 0; ich < nModules; ich++) channelGroups[ich] = 0;

    ((QnCorrectionsDetectorConfigurationChannels *) config)->SetChannelsScheme(channels, channelGroups, nullptr);
  };


    
}
#endif //FLOW_QNTASK_H
