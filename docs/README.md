# Flow Vector Library

Flow vector framework providing methods for applying an iterative data-based correction procedure explored in the paper [Effects of nonuniform acceptance in anisotropic flow measurements](https://journals.aps.org/prc/abstract/10.1103/PhysRevC.77.034904). It is based on the [FlowVectorCorrections](https://github.com/FlowCorrections/FlowVectorCorrections).

For more Information on the usage of the framework please have a look at the gitbook.  
​[**See documentation here**](https://trickle.gitbook.io/flow)



