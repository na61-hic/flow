# Using Flow

## Prerequisites

To make classes available to ROOT6 please it is possible to add them to the following environment variables. ROOT6 needs to be available for the framework to run.

#### Include path

```bash
export ROOT_INCLUDE_PATH="$ROOT_INCLUDE_PATH:/path/include:$ROOT_INCLUDE_PATH"
```

#### LD library path

```bash
export LD_LIBRARY_PATH="/path/lib/:$LD_LIBRARY_PATH"
```

#### Load ROOT

```bash
source /path/root6/bin/thisroot.sh
```

{% hint style="info" %}
#### STL needs to be available

Please be sure to have the correct version of the standard library available, when running the framework.

For kronos please see instruction on how to load a newer gcc version with the correct STL [here](building-flow.md#use-the-modules-version-on-cvmfs-it-gsi-de).
{% endhint %}

## Using the framework

### Implement Datafiller.h

To be able to work the corrections need the Data filled into in a `double *values`. This is done in the **`DataFiller`** class. This class has to methods, which need to be defined:

* `FillEventInfo(std::shared_ptr<VariableManager> &man)`
  * Defines how the event information is filled into the framework. It is called before the event cuts are applied.
* `FillDetectors(MapDetectors &channel, MapDetectors &tracks, const std::shared_ptr<VariableManager> &man)`
  * Defines how the track information is filled into the framework. It is called after the event cuts are applied. Manual track loops may be defined inside this function.

#### Sample Implementation

{% code-tabs %}
{% code-tabs-item title="DataFiller.h" %}
```cpp
namespace Qn {
class DataFiller {
 public:
  using MapDetectors = std::map<std::string, Detector>;
  explicit DataFiller(EventInfo *event) : event_(event) {}
  void FillEventInfo(const std::shared_ptr<VariableManager> &var_manager) {
    FillEventInfo(event, var_manager->GetVariableContainer());
  }
  void FillDetectors(MapDetectors &channel, MapDetectors &tracking,
                     const std::shared_ptr<VariableManager> &var_manager) {
    for (auto &dp : channel) { dp.second.FillData(); }
    for (auto &track : event_.tracks) {
      VAR::FillTrackInfo(track, var_manager->GetVariableContainer());
      for (auto &dp : tracking) { dp.second.FillData(); }
    }
  }
 private:
  EventInfo *event_;
};
}
```
{% endcode-tabs-item %}
{% endcode-tabs %}

### Correction step

The corrections are configured using a instance of the `CorrectionManager`. All variables are passed into the manager using a C-array of doubles `double *values`. The values are accessed using the `VariableManager`. It has an internal map associating the position in the values array to a std::string name for easy configuration. 

#### Defining a variable

To add a variable to the manager for internal use. The name and the position and length inside the variable array needs to be specified. Variables can have a length. For example a multiplicity by channel for a detector with 100 Channels can be implemented by storing the multiplicity by channel in order inside the values array. Inside the framework it can then be accessing without the neet to determine the length again.

```cpp
void AddVariable(const std::string &name, const int id, const int length);
```

####  Adding a detector

To add a detector a Name and a C-callable configuring the internal correction steps and the Variable Name of the phi values are required. Optionally axes and weights can be passed. Axes describe a for example kinematic dependence of a QVector.

```cpp
void AddDetector(const std::string &name, DetectorType type, const std::string &phi_name,
                 const std::string &weight_name = "Ones", const std::vector<Qn::Axis> &axes = {});
```

A detector may need to be configured using cuts on the detector variables. These can be added using the AddCut function.

#### Adding a cut 

A multidimensional cut on a detector with a given name can be added using the `AddCut` method. The dimensions of the array of variable names determines the dimensions of the cut. A C-callable with the correct number parameters can be used to define the selection.

```cpp
template<std::size_t N, typename FUNCTION>
void AddCut(const std::string &name, const char* const (&name_arr)[N], FUNCTION lambda)
```

 



### Configuring the correlation step

